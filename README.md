# Spark_Project

Project using PySpark to manipulate databases.

# Requirements

System:

- Java: openjdk-8-jdk-headless 
- Spark: spark-2.4.4-bin-hadoop2.7

Libraries:

- findspark
- pandas
- isoweek


# Resume

The project was developed in Jupyter Collab by.

It's possible to run the code with the following link: 

https://colab.research.google.com/drive/1CDCSU9TNRwVxJJ0WdXWXYug5AnmAqT-w?usp=sharing

**ALERT**!! YOU NEED TO LOG IN GOOGLE ACCOUNT TO RUN.

# Running

Running in Collab:

    1. For you to run the code on the link above you need to insert manually these 4 files:

    **Directory: Files**

    1. Orders.json
    2. Products.json
    3. Sellers.json
    4. Buyers.json

    2. After that you are ready to run all the lines of the code.


Locally:

    1. If you want to run locally, you must have all the requirements and in case it's your first installation, you need to set this 
    enviroment variables:

    - JAVA_HOME = {path}+file - > "/usr/lib/jvm/java-8-openjdk-amd64"
    - SPARK_HOME = "{path}+file - > "/content/spark-2.4.4-bin-hadoop2.7"

    2. And also have Jupyter notebook installed, you can do it by: pip install jupyter-notebook

    3. Import to your local machine and Run the ipynb.file

**Observations**: 

* _As there were some visual responses of databases and dataframes i chose use jupyter notebook instead of .py file, and as the test didn't mencioned anything about modularize the script (creating classes and functions) i chose only to do  the script that shows de results of the answers._


# Objective

The objective of this project consists in using PySpark in order to solve the following questions:

1. Generate an enriched file that contains all unified information.

2. Sales growth per seller weekly (find the difference between the current week analyzed and last week,
for every week of the year). The solution should be a dataset per seller and per week (use week in
format ISO 8601).

3. Find the percentage that each of the sellers has contributed to the total sales since always.

4. There are orders that do not have the information on who made the purchase, prepare a table that
has who could be the possible buyer of each of these orders. Explain the logic used to arrive at this
information. 

# Results

All the Questions were solved.
